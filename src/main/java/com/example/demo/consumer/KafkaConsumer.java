package com.example.demo.consumer;

import com.adidas.m3.pb.avro.models.Model;
import com.example.demo.mapper.AvroModelMapper;
import com.example.demo.repository.ModelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class KafkaConsumer {

    @Autowired
    private ModelRepository modelRepository;

    @KafkaListener(topics = "${spring.kafka.consumer.topic-model}", groupId = "${spring.kafka.consumer.group-id}")
    public void consume(@Payload Model avroModel) {
        com.example.demo.entity.Model model = AvroModelMapper.avroModelToModel(avroModel);
        modelRepository.save(model);
    }
}
