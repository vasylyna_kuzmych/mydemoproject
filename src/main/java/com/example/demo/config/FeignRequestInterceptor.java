package com.example.demo.config;

import com.example.demo.client.ApiTokenClient;
import com.example.demo.dto.api.ApiTokenRequest;
import com.example.demo.dto.api.ApiTokenResponse;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FeignRequestInterceptor implements RequestInterceptor {

    private final ApiTokenClient apiTokenClient;

    @Value("${CLIENT_ID}")
    private String clientId;
    @Value("${CLIENT_SECRET}")
    private String clientSecret;
    @Value("${SCOPE}")
    private String scope;
    @Value("${GRANT_TYPE}")
    private String grantType;


    public FeignRequestInterceptor(ApiTokenClient apiTokenClient) {
        this.apiTokenClient = apiTokenClient;
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        ApiTokenRequest tokenRequest = new ApiTokenRequest();
        tokenRequest.setClient_id(clientId);
        tokenRequest.setClient_secret(clientSecret);
        tokenRequest.setScope(scope);
        tokenRequest.setGrant_type(grantType);

        ApiTokenResponse tokenResponse = apiTokenClient.getToken(tokenRequest);
        requestTemplate.header("Authorization", "Bearer " + tokenResponse.getAccessToken());
    }
}
