package com.example.demo.controller;

import com.example.demo.dto.ModelDto;
import com.example.demo.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/models")
public class ModelController {
    @Autowired
    private ModelService modelService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_READER') or hasRole('ROLE_EDITOR')")
    public ResponseEntity<List<ModelDto>> getAllModels() {
        List<ModelDto> articles = modelService.getAllModels();
        return ResponseEntity.ok(articles);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_READER') or hasRole('ROLE_EDITOR')")
    public ResponseEntity<ModelDto> getModelById(@PathVariable Long id) {
        ModelDto modelDto = modelService.getModelById(id);
        return ResponseEntity.ok(modelDto);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_EDITOR')")
    public ModelDto createModel(@RequestBody ModelDto modelDto) {
        return modelService.createModel(modelDto);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_EDITOR')")
    public ResponseEntity<ModelDto> updateModel(@PathVariable Long id, @RequestBody ModelDto modelDto) {
        return ResponseEntity.ok(modelService.updateModel(id, modelDto));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_EDITOR')")
    public ResponseEntity<Void> deleteModel(@PathVariable Long id) {
        modelService.deleteModel(id);
        return ResponseEntity.noContent().build();
    }
}
