package com.example.demo.controller;

import com.example.demo.service.ApiFeignService;
import com.example.demo.service.ApiRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/endpoints")
public class ApiController {

    @Autowired
    private ApiRestService apiService;

    @Autowired
    private ApiFeignService feignService;

    @PostMapping("/synchWithRest")
    public ResponseEntity<String> synchronizationWithRest() {
        apiService.synchronizationWithRest();
        return ResponseEntity.ok("Models saved successfully");
    }

    @PostMapping("/synchWithFeign")
    public ResponseEntity<String> synchronizationWithFeign() {
        feignService.synchronizationWithFeign();
        return ResponseEntity.ok("Models saved successfully");
    }
}
