package com.example.demo.dto;

import com.example.demo.entity.Article;
import com.example.demo.entity.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ArticleDto extends ModelDto{
    private Long artId;
    private Long artMdlId;
    private Long artNumber;
    private Long artSeasonId;
    private String artSeason;
    private String artModelName;
    private String artCollectionPack;
    private String artColorwayName;
    private String artColorDesigner;
    private String artConstruction;
    private String artDesigner;
    private String artDeveloper;
    private String artFobActualStatus;
    private String artGraphicCode;
    private String artGraphicSeries;
    private boolean artDeleted;
    private boolean artLateAdd;

    public Article toArticleEntity() {
        Article article = new Article();

        article.setArtId(this.getArtId());
        article.setArtNumber(this.getArtNumber());
        article.setArtSeasonId(this.getArtSeasonId());
        article.setArtModelName(this.getArtModelName());
        article.setArtCollectionPack(this.getArtCollectionPack());
        article.setArtColorwayName(this.getArtColorwayName());
        article.setArtColorDesigner(this.getArtColorDesigner());
        article.setArtConstruction(this.getArtConstruction());
        article.setArtDesigner(this.getArtDesigner());
        article.setArtDeveloper(this.getArtDeveloper());
        article.setArtFobActualStatus(this.getArtFobActualStatus());
        article.setArtGraphicCode(this.getArtGraphicCode());
        article.setArtGraphicSeries(this.getArtGraphicSeries());
        article.setArtDeleted(this.isArtDeleted());
        article.setArtLateAdd(this.isArtLateAdd());
        article.setArtSeason(this.getArtSeason());

        Model model = new Model();
        model.setMdlId(this.getMdlId());
        model.setMdlName(this.getMdlName());
        model.setMdlNameLong(this.getMdlNameLong());
        model.setMdlNumber(this.getMdlNumber());
        model.setMdlMeasuresHeight(this.getMdlMeasuresHeight());
        model.setMdlMeasuresLength(this.getMdlMeasuresLength());
        model.setMdlMeasuresWeight(this.getMdlMeasuresWeight());
        model.setMdlMeasuresWidth(this.getMdlMeasuresWidth());
        model.setMdlArticlesReadyForRelease(this.isMdlArticlesReadyForRelease());
        model.setMdlDeleted(this.isMdlDeleted());
        model.setMdlLateAdd(this.isMdlLateAdd());
        model.setMdlClosure(this.getMdlClosure());
        model.setMdlCollection(this.getMdlCollection());
        model.setMdlDeveloper(this.getMdlDeveloper());
        model.setMdlBrand(this.getMdlBrand());
        model.setMdlSeason(this.getMdlDeveloper());

        article.setModel(model);

        return article;
    }
}
