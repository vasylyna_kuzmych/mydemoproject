package com.example.demo.dto.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ApiTokenResponse {
    @JsonProperty("access_token")
    private String accessToken;
}
