package com.example.demo.dto.api;

import lombok.Data;

@Data
public class ApiTokenRequest {
    private String client_id;
    private String client_secret;
    private String scope;
    private String grant_type;
}
