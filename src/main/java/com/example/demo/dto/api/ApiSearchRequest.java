package com.example.demo.dto.api;

import java.util.List;
import lombok.Data;

@Data
public class ApiSearchRequest {
    List<String> includedAttributesCode;

    public ApiSearchRequest() {
        List<String> fieldNames = List.of("mdlId", "mdlName", "mdlNameLong", "mdlNumber", "mdlMeasuresHeight",
                "mdlMeasuresLength", "mdlMeasuresWeight", "mdlMeasuresWidth", "mdlArticlesReadyForRelease",
                "mdlDeleted", "mdlLateAdd", "mdlClosure", "mdlCollection", "mdlDeveloper", "mdlSeason", "mdlBrand");

        setIncludedAttributesCode(fieldNames);
    }
}
