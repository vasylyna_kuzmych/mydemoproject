package com.example.demo.dto.api;

import com.example.demo.entity.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ApiModelsResponse {

    private ListModels _embedded;

    @Data
    public static class ListModels {
        private List<Model> modelModels;
    }
}