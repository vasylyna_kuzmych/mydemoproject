package com.example.demo.dto.api;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class BrandAndSeasonSearchRequest extends ApiSearchRequest {
    private List<String> cmbBrand;
    private List<String> cmbSeason;

    public BrandAndSeasonSearchRequest(List<String> cmbBrand, List<String> cmbSeason) {
        setCmbBrand(cmbBrand);
        setCmbSeason(cmbSeason);
    }
}
