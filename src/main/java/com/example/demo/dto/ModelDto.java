package com.example.demo.dto;

import com.example.demo.entity.Model;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ModelDto {
    private Long mdlId;
    private String mdlName;
    private String mdlNameLong;
    private String mdlNumber;
    private String mdlSeason;
    private Float mdlMeasuresHeight;
    private Float mdlMeasuresLength;
    private Float mdlMeasuresWeight;
    private Float mdlMeasuresWidth;
    private boolean mdlArticlesReadyForRelease;
    private boolean mdlDeleted;
    private boolean mdlLateAdd;
    private String mdlBrand;
    private String mdlClosure;
    private String mdlCollection;
    private String mdlDeveloper;

    public Model toModelEntity() {
        Model model = new Model();

        model.setMdlName(this.getMdlName());
        model.setMdlNameLong(this.getMdlNameLong());
        model.setMdlNumber(this.getMdlNumber());
        model.setMdlMeasuresHeight(this.getMdlMeasuresHeight());
        model.setMdlMeasuresLength(this.getMdlMeasuresLength());
        model.setMdlMeasuresWeight(this.getMdlMeasuresWeight());
        model.setMdlMeasuresWidth(this.getMdlMeasuresWidth());
        model.setMdlArticlesReadyForRelease(this.isMdlArticlesReadyForRelease());
        model.setMdlDeleted(this.isMdlDeleted());
        model.setMdlLateAdd(this.isMdlLateAdd());
        model.setMdlClosure(this.getMdlClosure());
        model.setMdlCollection(this.getMdlCollection());
        model.setMdlDeveloper(this.getMdlDeveloper());
        model.setMdlBrand(this.getMdlBrand());
        model.setMdlSeason(this.getMdlSeason());

        return model;
    }
}
