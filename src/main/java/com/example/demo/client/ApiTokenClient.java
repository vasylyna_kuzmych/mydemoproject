package com.example.demo.client;

import com.example.demo.config.FeignClientConfig;
import com.example.demo.dto.api.ApiTokenRequest;
import com.example.demo.dto.api.ApiTokenResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "ApiTokenClient", url = "${api.token-url}", configuration = FeignClientConfig.class)
public interface ApiTokenClient {

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ApiTokenResponse getToken(@ModelAttribute ApiTokenRequest tokenRequest);
}
