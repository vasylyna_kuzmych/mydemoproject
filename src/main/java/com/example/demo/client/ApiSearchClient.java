package com.example.demo.client;

import com.example.demo.config.FeignClientConfig;
import com.example.demo.dto.api.ApiSearchRequest;
import com.example.demo.entity.Model;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "ApiSearchClient", url = "${api.search-url}", configuration = FeignClientConfig.class)
public interface ApiSearchClient {
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    PagedModel<Model> getModels(@PageableDefault @SpringQueryMap Pageable pageable, @ModelAttribute ApiSearchRequest searchDto);
}
