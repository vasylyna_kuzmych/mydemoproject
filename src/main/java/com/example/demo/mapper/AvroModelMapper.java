package com.example.demo.mapper;

import com.example.demo.entity.Model;

public class AvroModelMapper {

    public static Model avroModelToModel(com.adidas.m3.pb.avro.models.Model avroModel) {
        Model model = new Model();

        model.setMdlSeason(avroModel.getMdlSeason() != null ?
                avroModel.getMdlSeason().toString() : null);
        model.setMdlBrand(avroModel.getMdlBrand() != null ?
                avroModel.getMdlBrand().toString() : null);
        model.setMdlName(avroModel.getMdlName() != null ?
                avroModel.getMdlName().toString() : null);
        model.setMdlNameLong(avroModel.getMdlNameLong() != null ?
                avroModel.getMdlNameLong().toString(): null);
        model.setMdlNumber(avroModel.getMdlNumber() != null ?
                avroModel.getMdlNumber().toString(): null);
        model.setMdlMeasuresHeight(avroModel.getMdlMeasuresHeight() != null ?
                avroModel.getMdlMeasuresHeight().floatValue() : null);
        model.setMdlMeasuresLength(avroModel.getMdlMeasuresLength() != null ?
                avroModel.getMdlMeasuresLength().floatValue() : null);
        model.setMdlMeasuresWeight(avroModel.getMdlMeasuresWeight() != null ?
                avroModel.getMdlMeasuresWeight().floatValue() : null);
        model.setMdlMeasuresWidth(avroModel.getMdlMeasuresWidth() != null ?
                avroModel.getMdlMeasuresWidth().floatValue() : null);
        model.setMdlArticlesReadyForRelease(avroModel.getMdlReadyForRelease());
        model.setMdlDeleted(avroModel.getMdlDeleted());
        model.setMdlLateAdd(avroModel.getMdlLateAdd());
        model.setMdlClosure(avroModel.getMdlClosure() != null ?
                avroModel.getMdlClosure().toString() : null);
        model.setMdlCollection(avroModel.getMdlCollection() != null ?
                avroModel.getMdlCollection().toString() : null);
        model.setMdlDeveloper(avroModel.getMdlDeveloper() != null ?
                avroModel.getMdlDeveloper().toString() : null);

        return model;
    }
}
