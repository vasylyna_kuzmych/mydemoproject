package com.example.demo.entity;

import com.example.demo.dto.ModelDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Data
public class Model extends RepresentationModel<Model> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long mdlId;
    private String mdlName;
    private String mdlNameLong;
    private String mdlNumber;
    private Float mdlMeasuresHeight;
    private Float mdlMeasuresLength;
    private Float mdlMeasuresWeight;
    private Float mdlMeasuresWidth;
    private boolean mdlArticlesReadyForRelease;
    private boolean mdlDeleted;
    private boolean mdlLateAdd;
    private String mdlClosure;
    private String mdlCollection;
    private String mdlDeveloper;
    private String mdlSeason;
    private String mdlBrand;

    @OneToMany(mappedBy = "model", cascade = CascadeType.ALL)
    private List<Article> articles;

    public ModelDto toModelDto() {
        ModelDto model = new ModelDto();

        model.setMdlId(this.getMdlId());
        model.setMdlName(this.getMdlName());
        model.setMdlNameLong(this.getMdlNameLong());
        model.setMdlNumber(this.getMdlNumber());
        model.setMdlMeasuresHeight(this.getMdlMeasuresHeight());
        model.setMdlMeasuresLength(this.getMdlMeasuresLength());
        model.setMdlMeasuresWeight(this.getMdlMeasuresWeight());
        model.setMdlMeasuresWidth(this.getMdlMeasuresWidth());
        model.setMdlArticlesReadyForRelease(this.isMdlArticlesReadyForRelease());
        model.setMdlDeleted(this.isMdlDeleted());
        model.setMdlLateAdd(this.isMdlLateAdd());
        model.setMdlClosure(this.getMdlClosure());
        model.setMdlCollection(this.getMdlCollection());
        model.setMdlDeveloper(this.getMdlDeveloper());
        model.setMdlSeason(this.getMdlSeason());
        model.setMdlBrand(this.getMdlBrand());

        return model;
    }
}
