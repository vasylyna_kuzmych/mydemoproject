package com.example.demo.entity;

import com.example.demo.dto.ArticleDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long artId;
    private Long artNumber;
    private Long artSeasonId;
    private String artModelName;
    private String artCollectionPack;
    private String artColorwayName;
    private String artColorDesigner;
    private String artConstruction;
    private String artDesigner;
    private String artDeveloper;
    private String artFobActualStatus;
    private String artGraphicCode;
    private String artGraphicSeries;
    private boolean artDeleted;
    private boolean artLateAdd;
    private String artSeason;

    @ManyToOne
    @JoinColumn(name = "artMdlId", nullable = false)
    private Model model;

    public ArticleDto toArticleDto() {
        ArticleDto article = new ArticleDto();

        article.setArtId(this.getArtId());
        article.setArtNumber(this.getArtNumber());
        article.setArtModelName(this.getArtModelName());
        article.setArtCollectionPack(this.getArtCollectionPack());
        article.setArtColorwayName(this.getArtColorwayName());
        article.setArtColorDesigner(this.getArtColorDesigner());
        article.setArtConstruction(this.getArtConstruction());
        article.setArtDesigner(this.getArtDesigner());
        article.setArtDeveloper(this.getArtDeveloper());
        article.setArtFobActualStatus(this.getArtFobActualStatus());
        article.setArtGraphicCode(this.getArtGraphicCode());
        article.setArtGraphicSeries(this.getArtGraphicSeries());
        article.setArtDeleted(this.isArtDeleted());
        article.setArtLateAdd(this.isArtLateAdd());
        article.setArtSeason(this.getArtSeason());

        if (this.model != null) {
            article.setArtMdlId(this.model.getMdlId());
            article.setMdlId(this.model.getMdlId());
            article.setMdlName(this.model.getMdlName());
            article.setMdlNameLong(this.model.getMdlNameLong());
            article.setMdlNumber(this.model.getMdlNumber());
            article.setMdlMeasuresHeight(this.model.getMdlMeasuresHeight());
            article.setMdlMeasuresLength(this.model.getMdlMeasuresLength());
            article.setMdlMeasuresWeight(this.model.getMdlMeasuresWeight());
            article.setMdlMeasuresWidth(this.model.getMdlMeasuresWidth());
            article.setMdlArticlesReadyForRelease(this.model.isMdlArticlesReadyForRelease());
            article.setMdlDeleted(this.model.isMdlDeleted());
            article.setMdlLateAdd(this.model.isMdlLateAdd());
            article.setMdlClosure(this.model.getMdlClosure());
            article.setMdlCollection(this.model.getMdlCollection());
            article.setMdlDeveloper(this.model.getMdlDeveloper());
            article.setMdlSeason(this.model.getMdlSeason());
            article.setMdlBrand(this.model.getMdlBrand());
        }

        return article;
    }
}
