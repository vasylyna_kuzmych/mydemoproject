package com.example.demo.entity;

public enum Role {
    ROLE_READER,
    ROLE_EDITOR
}
