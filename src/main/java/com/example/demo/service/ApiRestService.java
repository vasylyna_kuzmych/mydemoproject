package com.example.demo.service;

import com.example.demo.dto.api.ApiModelsResponse;
import com.example.demo.entity.Model;
import com.example.demo.repository.ModelRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class ApiRestService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ModelRepository modelRepository;

    @Value("${api.token-url}")
    private String tokenUrl;
    @Value("${api.search-url}")
    private String searchUrl;

    @Value("${CLIENT_ID}")
    private String clientId;
    @Value("${CLIENT_SECRET}")
    private String clientSecret;
    @Value("${SCOPE}")
    private String scope;
    @Value("${GRANT_TYPE}")
    private String grantType;

    public void synchronizationWithRest() {

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(getToken());
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(setJsonBody(), headers);
        List<Model> models = new ArrayList<>();

        for (int page = 0; page < 2; page++) {
            ResponseEntity<ApiModelsResponse> response = restTemplate.exchange(
                    searchUrl + "&size=10&page=" + page,
                    HttpMethod.POST,
                    entity,
                    ApiModelsResponse.class);

            models.addAll(Objects.requireNonNull(response.getBody()).get_embedded().getModelModels());
        }

        modelRepository.saveAll(models);
    }

    private String getToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("client_id", clientId);
        body.add("client_secret", clientSecret);
        body.add("scope", scope);
        body.add("grant_type", grantType);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<Map> response = restTemplate.postForEntity(tokenUrl, requestEntity, Map.class);

        return (String) Objects.requireNonNull(response.getBody()).get("access_token");
    }

    private String setJsonBody() {
        List<String> fieldNames = List.of("mdlId", "mdlName", "mdlNameLong", "mdlNumber", "mdlMeasuresHeight",
                "mdlMeasuresLength", "mdlMeasuresWeight", "mdlMeasuresWidth", "mdlArticlesReadyForRelease",
                "mdlDeleted", "mdlLateAdd", "mdlClosure", "mdlCollection", "mdlDeveloper", "mdlSeason", "mdlBrand");

        Map<String, Object> map = new HashMap<>();
        map.put("includedAttributesCode", fieldNames);

        String json;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return json;
    }
}
