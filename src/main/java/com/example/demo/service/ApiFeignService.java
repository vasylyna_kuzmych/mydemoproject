package com.example.demo.service;

import com.example.demo.client.ApiSearchClient;
import com.example.demo.client.ApiTokenClient;
import com.example.demo.dto.api.*;
import com.example.demo.entity.Model;
import com.example.demo.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ApiFeignService {

    @Autowired
    private ModelRepository modelRepository;
    @Autowired
    private ApiTokenClient apiTokenClient;
    @Autowired
    private ApiSearchClient apiSearchClient;

    public void synchronizationWithFeign() {
        List<Model> models = new ArrayList<>();
        List<String> cmbBrand = List.of("11");
        List<String> cmbSeason = List.of("20242");

        for (int page = 0; page < 2; page++) {
            Pageable pageable = PageRequest.of(page, 10);
            ApiSearchRequest searchDto = new BrandAndSeasonSearchRequest(cmbBrand, cmbSeason);
            PagedModel<Model> modelsResponse = apiSearchClient.getModels(pageable, searchDto);
            models.addAll(modelsResponse.getContent());
        }

        modelRepository.saveAll(models);
    }
}
