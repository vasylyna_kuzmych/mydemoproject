package com.example.demo.service;

import com.example.demo.dto.ArticleDto;
import com.example.demo.entity.Article;
import com.example.demo.entity.Model;
import com.example.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    public List<ArticleDto> getAllArticles() {
        List<Article> articles =  articleRepository.findAll();
        return articles.stream()
                .map(Article::toArticleDto)
                .collect(Collectors.toList());
    }

    public ArticleDto getArticleById(Long id) {
        Article article = articleRepository.findById(id).orElseThrow(() -> new RuntimeException("Article not found"));
        return article.toArticleDto();
    }

    public ArticleDto createArticle(ArticleDto articleDto) {
        Article article = articleRepository.save(articleDto.toArticleEntity());
        return article.toArticleDto();
    }

    public ArticleDto updateArticle(Long id, ArticleDto articleDetails) {
        Article article = articleRepository.findById(id).orElseThrow(() -> new RuntimeException("Article not found"));

        article.setArtSeason(articleDetails.getArtSeason());
        article.setArtNumber(articleDetails.getArtNumber());
        article.setArtModelName(articleDetails.getArtModelName());
        article.setArtCollectionPack(articleDetails.getArtCollectionPack());
        article.setArtColorwayName(articleDetails.getArtColorwayName());
        article.setArtColorDesigner(articleDetails.getArtColorDesigner());
        article.setArtConstruction(articleDetails.getArtConstruction());
        article.setArtDesigner(articleDetails.getArtDesigner());
        article.setArtDeveloper(articleDetails.getArtDeveloper());
        article.setArtFobActualStatus(articleDetails.getArtFobActualStatus());
        article.setArtGraphicCode(articleDetails.getArtGraphicCode());
        article.setArtGraphicSeries(articleDetails.getArtGraphicSeries());
        article.setArtDeleted(articleDetails.isArtDeleted());
        article.setArtLateAdd(articleDetails.isArtLateAdd());

        Model model = article.getModel();
        if (model != null) {
            model.setMdlSeason(articleDetails.getMdlSeason());
            model.setMdlBrand(articleDetails.getMdlBrand());
            model.setMdlId(articleDetails.getMdlId());
            model.setMdlName(articleDetails.getMdlName());
            model.setMdlNameLong(articleDetails.getMdlNameLong());
            model.setMdlNumber(articleDetails.getMdlNumber());
            model.setMdlMeasuresHeight(articleDetails.getMdlMeasuresHeight());
            model.setMdlMeasuresLength(articleDetails.getMdlMeasuresLength());
            model.setMdlMeasuresWeight(articleDetails.getMdlMeasuresWeight());
            model.setMdlMeasuresWidth(articleDetails.getMdlMeasuresWidth());
            model.setMdlArticlesReadyForRelease(articleDetails.isMdlArticlesReadyForRelease());
            model.setMdlDeleted(articleDetails.isMdlDeleted());
            model.setMdlLateAdd(articleDetails.isMdlLateAdd());
            model.setMdlClosure(articleDetails.getMdlClosure());
            model.setMdlCollection(articleDetails.getMdlCollection());
            model.setMdlDeveloper(articleDetails.getMdlDeveloper());
        }

        return articleRepository.save(article).toArticleDto();
    }

    public void deleteArticle(Long id) {
        articleRepository.deleteById(id);
    }
}
