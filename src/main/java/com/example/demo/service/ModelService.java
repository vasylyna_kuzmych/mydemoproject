package com.example.demo.service;

import com.example.demo.dto.ModelDto;
import com.example.demo.entity.Model;
import com.example.demo.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModelService {
    @Autowired
    private ModelRepository modelRepository;

    public List<ModelDto> getAllModels() {
        List<Model> models =  modelRepository.findAll();
        return models.stream()
                .map(Model::toModelDto)
                .collect(Collectors.toList());
    }

    public ModelDto getModelById(Long id) {
        Model model = modelRepository.findById(id).orElseThrow(() -> new RuntimeException("Model not found"));
        return model.toModelDto();
    }

    public boolean existsModelById(Long id) {
        return modelRepository.existsById(id);
    }

    public ModelDto createModel(ModelDto modelDto) {
        Model model = modelRepository.save(modelDto.toModelEntity());
        return model.toModelDto();
    }

    public ModelDto updateModel(Long id, ModelDto modelDetails) {
        Model model = modelRepository.findById(id).orElseThrow(() -> new RuntimeException("Model not found"));

        model.setMdlSeason(modelDetails.getMdlSeason());
        model.setMdlBrand(modelDetails.getMdlBrand());
        model.setMdlName(modelDetails.getMdlName());
        model.setMdlNameLong(modelDetails.getMdlNameLong());
        model.setMdlNumber(modelDetails.getMdlNumber());
        model.setMdlMeasuresHeight(modelDetails.getMdlMeasuresHeight());
        model.setMdlMeasuresLength(modelDetails.getMdlMeasuresLength());
        model.setMdlMeasuresWeight(modelDetails.getMdlMeasuresWeight());
        model.setMdlMeasuresWidth(modelDetails.getMdlMeasuresWidth());
        model.setMdlArticlesReadyForRelease(modelDetails.isMdlArticlesReadyForRelease());
        model.setMdlDeleted(modelDetails.isMdlDeleted());
        model.setMdlLateAdd(modelDetails.isMdlLateAdd());
        model.setMdlClosure(modelDetails.getMdlClosure());
        model.setMdlCollection(modelDetails.getMdlCollection());
        model.setMdlDeveloper(modelDetails.getMdlDeveloper());

        return modelRepository.save(model).toModelDto();
    }

    public void deleteModel(Long id) {
        modelRepository.deleteById(id);
    }
}
