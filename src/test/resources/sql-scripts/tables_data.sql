INSERT INTO model (mdl_name, mdl_name_long, mdl_number, mdl_measures_height, mdl_measures_length, mdl_measures_weight, mdl_measures_width, mdl_articles_ready_for_release, mdl_deleted, mdl_late_add, mdl_closure, mdl_collection, mdl_developer, mdl_season, mdl_brand)
VALUES ('Test Model1', 'Test Model1 Long', '12345', 10.5, 20.0, 5.0, 15.0, true, false, false, 'Closure', 'Collection', 'Developer', 'Season', 'Brand');
INSERT INTO model (mdl_name, mdl_name_long, mdl_number, mdl_measures_height, mdl_measures_length, mdl_measures_weight, mdl_measures_width, mdl_articles_ready_for_release, mdl_deleted, mdl_late_add, mdl_closure, mdl_collection, mdl_developer, mdl_season, mdl_brand)
VALUES ('Test Model2', 'Test Model2 Long', '12345', 10.5, 20.0, 5.0, 15.0, true, false, false, 'Closure', 'Collection', 'Developer', 'Season', 'Brand');
INSERT INTO model (mdl_name, mdl_name_long, mdl_number, mdl_measures_height, mdl_measures_length, mdl_measures_weight, mdl_measures_width, mdl_articles_ready_for_release, mdl_deleted, mdl_late_add, mdl_closure, mdl_collection, mdl_developer, mdl_season, mdl_brand)
VALUES ('Test Model3', 'Test Model3 Long', '12345', 10.5, 20.0, 5.0, 15.0, true, false, false, 'Closure', 'Collection', 'Developer', 'Season', 'Brand');
INSERT INTO model (mdl_name, mdl_name_long, mdl_number, mdl_measures_height, mdl_measures_length, mdl_measures_weight, mdl_measures_width, mdl_articles_ready_for_release, mdl_deleted, mdl_late_add, mdl_closure, mdl_collection, mdl_developer, mdl_season, mdl_brand)
VALUES ('Test Model4', 'Test Model3 Long', '12345', 10.5, 20.0, 5.0, 15.0, true, false, false, 'Closure', 'Collection', 'Developer', 'Season', 'Brand');
INSERT INTO model (mdl_name, mdl_name_long, mdl_number, mdl_measures_height, mdl_measures_length, mdl_measures_weight, mdl_measures_width, mdl_articles_ready_for_release, mdl_deleted, mdl_late_add, mdl_closure, mdl_collection, mdl_developer, mdl_season, mdl_brand)
VALUES ('Test Model5', 'Test Model3 Long', '12345', 10.5, 20.0, 5.0, 15.0, true, false, false, 'Closure', 'Collection', 'Developer', 'Season', 'Brand');

INSERT INTO users (username, email, password, role)
VALUES ('reader', 'reader@example.com', '$2a$10$HPTn4K1JRUGEAZA8AbPOfOx1KrrG.e9yzuFzFpd19/FYpWE9w1Z2G', 'ROLE_READER');
INSERT INTO users (username, email, password, role)
VALUES ('editor', 'editor@example.com', '$2a$10$HPTn4K1JRUGEAZA8AbPOfOx1KrrG.e9yzuFzFpd19/FYpWE9w1Z2G', 'ROLE_EDITOR');
