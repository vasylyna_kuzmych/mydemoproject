DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS model;

CREATE TABLE model (
    mdl_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    mdl_name VARCHAR(255),
    mdl_name_long VARCHAR(255),
    mdl_number VARCHAR(255),
    mdl_measures_height FLOAT,
    mdl_measures_length FLOAT,
    mdl_measures_weight FLOAT,
    mdl_measures_width FLOAT,
    mdl_articles_ready_for_release BOOLEAN,
    mdl_deleted BOOLEAN,
    mdl_late_add BOOLEAN,
    mdl_closure VARCHAR(255),
    mdl_collection VARCHAR(255),
    mdl_developer VARCHAR(255),
    mdl_season VARCHAR(255),
    mdl_brand VARCHAR(255)
);

CREATE TABLE article (
    art_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    art_number BIGINT,
    art_season_id BIGINT,
    art_model_name VARCHAR(255),
    art_collection_pack VARCHAR(255),
    art_colorway_name VARCHAR(255),
    art_color_designer VARCHAR(255),
    art_construction VARCHAR(255),
    art_designer VARCHAR(255),
    art_developer VARCHAR(255),
    art_fob_actual_status VARCHAR(255),
    art_graphic_code VARCHAR(255),
    art_graphic_series VARCHAR(255),
    art_deleted BOOLEAN,
    art_late_add BOOLEAN,
    art_season VARCHAR(255),
    art_mdl_id BIGINT,
    FOREIGN KEY (art_mdl_id) REFERENCES model(mdl_id)
);

CREATE TABLE users (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255),
    role VARCHAR(50) CHECK (role IN ('ROLE_EDITOR', 'ROLE_READER'))
);