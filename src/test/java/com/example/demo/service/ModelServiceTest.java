package com.example.demo.service;

import com.example.demo.dto.ModelDto;
import com.example.demo.entity.Model;
import com.example.demo.repository.ModelRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class ModelServiceTest {

    @Mock
    private ModelRepository modelRepository;

    @InjectMocks
    private ModelService modelService;

    @Test
    void testGetAllModels() {
        Model model1 = setModel(1L);
        Model model2 = setModel(2L);
        List<Model> testModelEntities = List.of(model1, model2);

        when(modelRepository.findAll()).thenReturn(testModelEntities);

        List<ModelDto> models = modelService.getAllModels();

        assertEquals(2, models.size());
        assertEquals("Model X1", models.get(0).getMdlName());
        assertEquals("Model X2", models.get(1).getMdlName());
    }

    @Test
    void testGetModelById() {
        Model model = setModel(1L);

        when(modelRepository.findById(1L)).thenReturn(Optional.of(model));

        ModelDto modelDto = modelService.getModelById(1L);

        assertEquals(1L, modelDto.getMdlId());
        assertEquals("Model X1", modelDto.getMdlName());
    }

    @Test
    void testCreateModel() {
        Model model = setModel(1L);
        ModelDto modelDto = setModelDto(1L);

        when(modelRepository.save(any(Model.class))).thenReturn(model);

        ModelDto createdModel = modelService.createModel(modelDto);

        assertEquals(1L, modelDto.getMdlId());
        assertEquals("ModelDto X1", modelDto.getMdlName());
    }

    @Test
    void updateModel() {
        Model model = setModel(1L);
        ModelDto modelDto = setModelDto(1L);

        when(modelRepository.findById(1L)).thenReturn(Optional.of(model));
        when(modelRepository.save(any(Model.class))).thenReturn(model);

        ModelDto updatedModel = modelService.updateModel(1L, modelDto);

        assertEquals("ModelDto X1", updatedModel.getMdlName());
    }

    @Test
    void deleteModel() {
        doNothing().when(modelRepository).deleteById(1L);

        modelService.deleteModel(1L);

        verify(modelRepository, times(1)).deleteById(1L);
    }

    private Model setModel(Long id) {
        Model model = new Model();

        model.setMdlId(id);
        model.setMdlName("Model X" + id);
        model.setMdlNameLong("Model X Long Description");
        model.setMdlNumber("MX123456");
        model.setMdlMeasuresHeight(10.5F);
        model.setMdlMeasuresLength(20.5F);
        model.setMdlMeasuresWeight(1.0F);
        model.setMdlMeasuresWidth(5.0F);
        model.setMdlArticlesReadyForRelease(true);
        model.setMdlDeleted(false);
        model.setMdlLateAdd(true);
        model.setMdlClosure("Zipper");
        model.setMdlCollection("Spring Collection");
        model.setMdlDeveloper("Jane Doe");
        model.setMdlSeason("Spring 2023");
        model.setMdlBrand("BrandX");

        return model;
    }

    private ModelDto setModelDto(Long id) {
        ModelDto model = new ModelDto();

        model.setMdlId(id);
        model.setMdlName("ModelDto X" + id);
        model.setMdlNameLong("Model X Long Description");
        model.setMdlNumber("MX123456");
        model.setMdlMeasuresHeight(10.5F);
        model.setMdlMeasuresLength(20.5F);
        model.setMdlMeasuresWeight(1.0F);
        model.setMdlMeasuresWidth(5.0F);
        model.setMdlArticlesReadyForRelease(true);
        model.setMdlDeleted(false);
        model.setMdlLateAdd(true);
        model.setMdlClosure("Zipper");
        model.setMdlCollection("Spring Collection");
        model.setMdlDeveloper("Jane Doe");
        model.setMdlSeason("Spring 2023");
        model.setMdlBrand("BrandX");

        return model;
    }
}