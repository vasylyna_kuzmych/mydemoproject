package com.example.demo.controller;

import com.example.demo.entity.Model;
import com.example.demo.security.SecurityConfig;
import com.example.demo.service.ModelService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Isolated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.security.Key;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(SecurityConfig.class)
@Sql(scripts = "/sql-scripts/tables_schema.sql")
@Sql(scripts = "/sql-scripts/tables_data.sql")
@ActiveProfiles("test")
@Isolated
public class ModelControllerIntegrationTest {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ModelService modelService;

    private final String USER_READER = "reader";
    private final String USER_EDITOR = "editor";

    @Test
    public void testGetAllModels() {
        HttpEntity<String> entity = new HttpEntity<>(null, getHeaders(USER_READER));

        //when
        ResponseEntity<List> response = restTemplate.exchange(
                getBaseUrl(),
                HttpMethod.GET,
                entity,
                List.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Model> models = response.getBody();
        assertNotNull(models);
        assertEquals(5, models.size());
    }

    @Test
    public void testGetModelById() {
        HttpEntity<String> entity = new HttpEntity<>(null, getHeaders(USER_READER));

        //when
        ResponseEntity<Model> response = restTemplate.exchange(
                getBaseUrl() + "/1",
                HttpMethod.GET,
                entity,
                Model.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Model model = response.getBody();
        assertNotNull(model);
        assertEquals(1, model.getMdlId());
    }

    @Test
    public void testCreateModelForReaderIsForbidden() {
        String modelName = "New Model";
        Model newModel = setModel(modelName);
        HttpEntity<Model> entity = new HttpEntity<>(newModel, getHeaders(USER_READER));

        //when
        ResponseEntity<Model> response = restTemplate.exchange(
                getBaseUrl(),
                HttpMethod.POST,
                entity,
                Model.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    @Test
    public void testCreateModel() {
        String modelName = "New Model";
        Model newModel = setModel(modelName);
        HttpEntity<Model> entity = new HttpEntity<>(newModel, getHeaders(USER_EDITOR));

        //when
        ResponseEntity<Model> response = restTemplate.exchange(
                getBaseUrl(),
                HttpMethod.POST,
                entity,
                Model.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Model model = response.getBody();
        assertNotNull(model);
        assertEquals(modelName, model.getMdlName());
    }

    @Test
    public void testUpdateModel() {
        String modelName = "Updated Model";
        Model updatedModel = setModel(modelName);
        HttpEntity<Model> entity = new HttpEntity<>(updatedModel, getHeaders(USER_EDITOR));

        //when
        ResponseEntity<Model> response = restTemplate.exchange(
                getBaseUrl() + "/1",
                HttpMethod.PUT,
                entity,
                Model.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Model model = response.getBody();
        assertNotNull(model);
        assertEquals(modelName, model.getMdlName());
    }

    @Test
    public void testDeleteModel() {
        HttpEntity<Model> entity = new HttpEntity<>(null, getHeaders(USER_EDITOR));

        //when
        ResponseEntity<Void> response = restTemplate.exchange(
                getBaseUrl() + "/1",
                HttpMethod.DELETE,
                entity,
                Void.class);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertFalse(modelService.existsModelById(1L));
    }

    private String getBaseUrl() {
        return "http://localhost:" + port + "/api/models";
    }

    private HttpHeaders getHeaders(String username) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + generateJwt(username));

        return headers;
    }

    public String generateJwt(String username) {
        Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));

        return Jwts.builder()
                .setSubject(username)
                .signWith(key)
                .compact();
    }

    private static Model setModel(String name) {
        Model model = new Model();

        model.setMdlName(name);
        model.setMdlNameLong("Model X Long Description");
        model.setMdlNumber("MX123456");
        model.setMdlMeasuresHeight(10.5F);
        model.setMdlMeasuresLength(20.5F);
        model.setMdlMeasuresWeight(1.0F);
        model.setMdlMeasuresWidth(5.0F);
        model.setMdlArticlesReadyForRelease(true);
        model.setMdlDeleted(false);
        model.setMdlLateAdd(true);
        model.setMdlClosure("Zipper");
        model.setMdlCollection("Spring Collection");
        model.setMdlDeveloper("Jane Doe");
        model.setMdlSeason("Spring 2023");
        model.setMdlBrand("BrandX");

        return model;
    }
}
