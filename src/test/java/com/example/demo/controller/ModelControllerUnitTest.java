package com.example.demo.controller;

import com.example.demo.dto.ModelDto;
import com.example.demo.security.SecurityConfig;
import com.example.demo.security.jwt.JwtUtils;
import com.example.demo.service.ModelService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

@WebMvcTest(ModelController.class)
@Import(SecurityConfig.class)
class ModelControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ModelService modelService;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private UserDetailsService userDetailsService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllModelsUnauthorized() throws Exception {
        mockMvc.perform(get("/api/models"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "READER")
    public void testCreateModelForbidden() throws Exception {
        mockMvc.perform(post("/api/models")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"mdlId\": 1}"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = {"READER", "EDITOR"})
    void testGetAllModels() throws Exception {
        ModelDto model1 = setModelDto(1L);
        ModelDto model2 = setModelDto(2L);
        List<ModelDto> testModels = List.of(model1, model2);

        when(modelService.getAllModels()).thenReturn(testModels);

        mockMvc.perform(get("/api/models"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].mdlId").value(1L))
                .andExpect(jsonPath("$[0].mdlName").value("Model X1"))
                .andExpect(jsonPath("$[1].mdlId").value(2L))
                .andExpect(jsonPath("$[1].mdlName").value("Model X2"));
    }

    @Test
    @WithMockUser(roles = {"READER", "EDITOR"})
    void testGetModelById() throws Exception {
        ModelDto model = setModelDto(1L);

        when(modelService.getModelById(anyLong())).thenReturn(model);

        mockMvc.perform(get("/api/models/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.mdlId").value(1L))
                .andExpect(jsonPath("$.mdlName").value("Model X1"));
    }

    @Test
    @WithMockUser(roles = "EDITOR")
    void testCreateModel() throws Exception {
        ModelDto model = setModelDto(1L);

        when(modelService.createModel(any(ModelDto.class))).thenReturn(model);

        ObjectMapper mapper = new ObjectMapper();
        String modelJson = mapper.writeValueAsString(model);

        mockMvc.perform(post("/api/models")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(modelJson))

                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "EDITOR")
    void testUpdateModel() throws Exception {
        ModelDto model = setModelDto(1L);

        when(modelService.updateModel(anyLong(), any(ModelDto.class))).thenReturn(model);

        mockMvc.perform(put("/api/models/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"mdlName\": \"Model X1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.mdlId").value(1L))
                .andExpect(jsonPath("$.mdlName").value("Model X1"));
    }

    @Test
    @WithMockUser(roles = "EDITOR")
    void testDeleteModel() throws Exception {
        mockMvc.perform(delete("/api/models/1"))
                .andExpect(status().isNoContent());
    }

    private static ModelDto setModelDto(Long id) {
        ModelDto model = new ModelDto();

        model.setMdlId(id);
        model.setMdlName("Model X" + id);
        model.setMdlNameLong("Model X Long Description");
        model.setMdlNumber("MX123456");
        model.setMdlMeasuresHeight(10.5F);
        model.setMdlMeasuresLength(20.5F);
        model.setMdlMeasuresWeight(1.0F);
        model.setMdlMeasuresWidth(5.0F);
        model.setMdlArticlesReadyForRelease(true);
        model.setMdlDeleted(false);
        model.setMdlLateAdd(true);
        model.setMdlClosure("Zipper");
        model.setMdlCollection("Spring Collection");
        model.setMdlDeveloper("Jane Doe");
        model.setMdlSeason("Spring 2023");
        model.setMdlBrand("BrandX");

        return model;
    }
}